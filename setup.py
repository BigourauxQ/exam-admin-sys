from setuptools import setup
import setuptools

setup(
    name = 'ROBOT',
    version = '0.0.1',
    author = "BIGOURAUX Quentin ",
    packages = ['ROBOT'],
    description = 'ROBOT est un package qui permet d intérargir avec des positions',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    license = 'GNU GPLv3',
    python_requires = '>=2',
)
