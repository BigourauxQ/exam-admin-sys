""" code de test unitaire testant chaque fonction de la class Grid """
#!/usr/bin/env python
# coding: utf-8

import unittest
from Space.grid_ import Grid

# import logging

##TEST##
class PositionTest(unittest.TestCase):
    """ classe de test unistaire """

    def setUp(self):
        """Executed before every test case"""
        self.grid = Grid()

    def test_addposition(self):
        """ test ajout position """
        result = self.grid.addposition("id1", 5.0, 5.0, 5.0)
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.grid.addposition("id2", 10.0, 10.0, 10.0)
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.grid.addposition("id2", 5.0, 10.0, 10.0)
        self.assertEqual(result, False)  # id deja utilise
        result = self.grid.addposition("id3", 5.0, 10.0, -10.0)
        self.assertEqual(result, False)  # rayon_r < 0
        result = self.grid.addposition("id3", 5.0, "5", 10.0)
        self.assertEqual(result, False)  # toutes les coordonnees ne sont pas des floats

    def test_removeposition(self):
        """ test remove"""
        self.grid.addposition("id1", 5.0, 5.0, 5.0)
        result = self.grid.removeposition("id1")
        self.assertEqual(result, True)  # fonctionnementg normal
        result = self.grid.removeposition("id")
        self.assertEqual(result, False)  # id non present

    def test_listposition(self):
        """ test de distance"""
        self.grid.addposition("id1", 5.0, 5.0, 5.0)
        self.grid.addposition("id2", 5.0, 6.0, 5.0)
        result = self.grid.listposition()
        self.assertEqual(result, [[5.0, 5.0, 5.0, "id1"], [5.0, 6.0, 5.0, "id2"]])

    def test_position(self):
        """ test le retour de position unique"""
        self.grid.addposition("id1", 5.0, 5.0, 5.0)
        self.grid.addposition("id2", 5.0, 6.0, 5.0)
        result = self.grid.position("id1")
        self.assertEqual(result, [5.0, 5.0, 5.0])
        result = self.grid.position("id2")
        self.assertEqual(result, [5.0, 6.0, 5.0])
        result = self.grid.position("id3")
        self.assertEqual(result, False)

    def test_distance(self):
        """ test la distance entre 2 points"""
        self.grid.addposition("id1", 5.0, 5.0, 5.0)
        self.grid.addposition("id2", 5.0, 6.0, 5.0)
        result = self.grid.distance("id1", "id2")
        self.assertEqual(result, 1.0)
        self.grid.addposition("id3", 5.0, 10.0, 5.0)
        result = self.grid.distance("id1", "id3")
        self.assertEqual(result, 5.0)
        result = self.grid.distance("id1", "id4")
        self.assertEqual(result, False)

    def test_de_collision(self):
        """test si il y a collision entre 2 points"""
        self.grid.addposition("id1", 5.0, 5.0, 5.0)
        self.grid.addposition("id2", 5.0, 6.0, 5.0)
        result = self.grid.collision("id1", "id2")
        self.assertEqual(result, True)
        self.grid.addposition("id3", 5.0, 10.0, 5.0)
        result = self.grid.collision("id1", "id3")
        self.assertEqual(result, False)
        result = self.grid.collision("id1", "id4")
        self.assertEqual(result, False)


if __name__ == "__main__":
    unittest.main()
