""" code de test unitaire testant chaque fonction de la class safegrid """
#!/usr/bin/env python
# coding: utf-8

import unittest
from Space.safegrid_ import SafeGrid

# import logging

##TEST##
class PositionTest(unittest.TestCase):
    """ classe de test unistaire """

    def setUp(self):
        """Executed before every test case"""
        self.safegrid = SafeGrid()

    def test_safeaddposition(self):
        """ test ajout position sans collision """
        result = self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.safegrid.safeaddposition("id2", 10.0, 10.0, 5.0)
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.safegrid.safeaddposition("id2", 5.0, 10.0, 10.0)
        self.assertEqual(result, False)  # id deja utilise
        result = self.safegrid.safeaddposition("id3", 5.0, 10.0, -10.0)
        self.assertEqual(result, False)  # rayon_r < 0
        result = self.safegrid.safeaddposition("id3", 5.0, "5", 10.0)
        self.assertEqual(result, False)  # toutes les coordonnees ne sont pas des floats
        result = self.safegrid.safeaddposition("id4", 5.0, 6.0, 10.0)
        self.assertEqual(result, False)  # il y a collision

    def test_safenewpossition(self):
        """ test la fonction de modification de la position"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.safegrid.safeaddposition("id2", 10.0, 10.0, 5.0)
        result = self.safegrid.safenewposition("id1", 5.0, 0.0)
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.safegrid.safenewposition("id2", 5.0, 0.0)
        self.assertEqual(result, False)  # collision avec id1
        result = self.safegrid.safenewposition("id1", 10.0, 10.0)
        self.assertEqual(result, False)  # collision avec id2

    def test_removeposition(self):
        """ test remove"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        result = self.safegrid.removeposition("id1")
        self.assertEqual(result, True)  # fonctionnement normal
        result = self.safegrid.removeposition("id")
        self.assertEqual(result, False)  # id non present

    def test_listposition(self):
        """ test de distance"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.safegrid.safeaddposition("id2", 5.0, 6.0, 5.0)
        result = self.safegrid.listposition()
        self.assertEqual(result, [[5.0, 5.0, 5.0, "id1"]])
        self.safegrid.safeaddposition("id2", 5.0, 15.0, 5.0)
        result = self.safegrid.listposition()
        self.assertEqual(result, [[5.0, 5.0, 5.0, "id1"], [5.0, 15.0, 5.0, "id2"]])

    def test_position(self):
        """test le retour de position unique"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.safegrid.safeaddposition("id2", 5.0, 6.0, 5.0)
        result = self.safegrid.position("id1")
        self.assertEqual(result, [5.0, 5.0, 5.0])
        result = self.safegrid.position("id2")
        self.assertEqual(
            result, False
        )  # id2 entre en collision avec id2 il ne peut donc pas etre cree
        result = self.safegrid.position("id3")
        self.assertEqual(result, False)
        self.safegrid.safeaddposition("id2", 5.0, 15.0, 5.0)
        result = self.safegrid.position("id2")
        self.assertEqual(result, [5.0, 15.0, 5.0])

    def test_distance(self):
        """test la distance entre 2 points"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.safegrid.safeaddposition("id2", 5.0, 6.0, 5.0)
        result = self.safegrid.distance("id1", "id2")
        self.assertEqual(
            result, False
        )  # id2 entre en collision avec id2 il ne peut donc pas etre cree
        self.safegrid.safeaddposition("id3", 5.0, 10.0, 5.0)
        result = self.safegrid.distance("id1", "id3")
        self.assertEqual(result, 5.0)
        result = self.safegrid.distance("id1", "id4")
        self.assertEqual(result, False)

    def test_de_collision(self):
        """test si il y a collision entre 2 points"""
        self.safegrid.safeaddposition("id1", 5.0, 5.0, 5.0)
        self.safegrid.safeaddposition("id2", 5.0, 6.0, 5.0)
        result = self.safegrid.collision("id1", "id2")
        self.assertEqual(result, False)  # id2 ne peut etre placee car collision
        self.safegrid.safeaddposition("id3", 5.0, 10.0, 5.0)
        result = self.safegrid.collision("id1", "id3")
        self.assertEqual(result, False)  # pas de collision
        result = self.safegrid.collision("id1", "id4")
        self.assertEqual(result, False)  # id4 n'existe pas donc pas de collision


if __name__ == "__main__":
    unittest.main()
