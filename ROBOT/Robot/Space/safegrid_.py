""" code ajoutant des position """
#!/usr/bin/env python
# coding: utf-8


from math import sqrt
import copy


class SafeGrid(object):
    """ classe pour ajouter supprimer des position, trouver des distances et de collision"""

    def __init__(self):
        self.positioncoord = {}

    def safeaddposition(self, id_, coord_x, coord_y, rayon_r):
        """ ajoute une position"""
        if id_ in self.positioncoord:  # si l'id existe deja
            return False
        elif (
                isinstance(coord_x, float)
                and isinstance(coord_y, float)
                and isinstance(rayon_r, float)
                and rayon_r > 0
        ):
            # on verifie que toures les conditions soit valide
            self.positioncoord[id_] = [coord_x, coord_y, rayon_r]  # on met la position
            for i in self.positioncoord:
                if id_ != i:  # si l'id est different de la position i du dictionnaire

                    collision = self.collision(
                        id_, i
                    )  # on verifie si il y a collision entre les deux

                    if (
                            collision
                    ):  # si il y a collision on enleve la position que l'on vient de mettre
                        self.removeposition(id_)
                        return False

            return True  # sinon on laisse la position que l'on vient de mettre
        return False

    def safenewposition(self, id_, newx, newy):
        """ deplace la position si cela implique aucune collision avec les autres positions"""
        # on enregistre les anciennes coordonnees
        old_x = self.positioncoord[id_][0]
        old_y = self.positioncoord[id_][1]
        old_r = self.positioncoord[id_][2]
        # on enleve la coordonnees a deplacer
        self.removeposition(id_)
        # on l'implemente a son nouvelle emplacement
        implante = self.safeaddposition(id_, newx, newy, old_r)
        # si c'est impossible, on recree la position anciennement supprime
        if not implante:
            self.positioncoord[id_] = [old_x, old_y, old_r]
            return False
        return True

    def removeposition(self, id_):
        """ enleve une position"""

        if id_ in self.positioncoord:  # si l'id associe a la position est valide
            del self.positioncoord[id_]
            return True

        return False

    def listposition(self):
        """ list les positions"""
        listpos = []

        for i in self.positioncoord:
            pos = []
            pos = copy.deepcopy(self.positioncoord[i])  # on recupaire les coordonnees
            pos.append(i)  # puis on ajout l'id a la fin
            listpos.append(pos)
        return listpos

    def position(self, id_):
        """ return une position precise"""
        if id_ in self.positioncoord:
            pos = self.positioncoord[id_]
            return pos

        return False

    def distance(self, id_1, id_2):
        """ donne la distance entre 2 pts"""
        if id_1 in self.positioncoord and id_2 in self.positioncoord:
            distance = sqrt(
                (self.positioncoord.get(id_2)[0] - self.positioncoord.get(id_1)[0]) ** 2
                + (self.positioncoord.get(id_2)[1] - self.positioncoord.get(id_1)[1])
                ** 2
            )
            return distance

        return False

    def collision(self, id_1, id_2):
        """indique si il y a collision"""
        distance = self.distance(id_1, id_2)

        # si une coordonnee se trouve dans le rayon de collision d'une autre
        # ( si la distance qui les separes est inferieur au rayon de colision de l'une ou l'autre)
        # alors on considere qu'il y a collision

        if isinstance(distance, float) and (
                distance < self.positioncoord.get(id_1)[2]
                or distance < self.positioncoord.get(id_2)[2]
        ):
            return True
        return False
