""" code ajoutant des position """
#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function

from math import sqrt
import copy


class Grid(object):
    """ classe pour ajouter supprimer des position, trouver des distances et de collision"""

    def __init__(self):
        self.positioncoord = {}

    def addposition(self, id_, coord_x, coord_y, rayon_r):
        # ces noms de varianles sont acceptees par pylint
        """ ajoute une position"""
        if id_ in self.positioncoord:

            return False

        elif (
                isinstance(coord_x, float)
                and isinstance(coord_y, float)
                and isinstance(rayon_r, float)
                and rayon_r > 0
        ):
            self.positioncoord[id_] = [coord_x, coord_y, rayon_r]

            return True

        return False

    def removeposition(self, id_):
        """ enleve une position"""

        if id_ in self.positioncoord:  # si l'id associe a la position est valide
            del self.positioncoord[id_]
            return True

        return False

    def listposition(self):
        """ list les positions"""
        listpos = []

        for i in self.positioncoord:
            pos = []
            pos = copy.deepcopy(self.positioncoord[i])  # on clone les coordonnees
            pos.append(i)  # puis on ajout l'id a la fin
            listpos.append(pos)

        return listpos

    def position(self, id_):
        """ return une position precise"""
        if id_ in self.positioncoord:
            pos = self.positioncoord[id_]
            return pos

        return False

    def distance(self, id_1, id_2):
        """ donne la distance entre 2 pts"""
        if id_1 in self.positioncoord and id_2 in self.positioncoord:
            distance = sqrt(
                (self.positioncoord.get(id_2)[0] - self.positioncoord.get(id_1)[0]) ** 2
                + (self.positioncoord.get(id_2)[1] - self.positioncoord.get(id_1)[1])
                ** 2
            )
            return distance

        return False

    def collision(self, id_1, id_2):
        """indique si il y a collision"""
        distance = self.distance(id_1, id_2)

        # si une coordonnee se trouve dans le rayon de collision d'une autre
        # ( si la distance qui les separes est inferieur au rayon de colision de l'une ou l'autre)
        # alors on considere qu'il y a collision

        if isinstance(distance, float) and (
                distance < self.positioncoord.get(id_1)[2]
                or distance < self.positioncoord.get(id_2)[2]
        ):
            return True
        return False
